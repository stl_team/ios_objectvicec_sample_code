

#import "GlobalMethods.h"
#import "AppConstants.h"

@implementation GlobalMethods

@synthesize isLoggedIn,showPass,showTicket,cartRect;

static GlobalMethods *sharedInstance;

- (id)init
{
    self = [super init];
    if (self)
    {
        //Custom Intialization
        isLoggedIn = NO;
        showTicket = NO;
        showPass = NO;
    }
    
    return self;
}

+ (GlobalMethods *)sharedInstance
{
    @synchronized(self)
    {
        if (sharedInstance == NULL)
        {
            sharedInstance = [[GlobalMethods alloc] init];
        }
        return sharedInstance;
    }
}

#pragma mark
#pragma mark - Show Alert Message
-(void)showAlertWithTitle:(NSString *)strTitle andMessage:(NSString *)strMsg withController:(UIViewController *)controller
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:strTitle  message:strMsg preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
       {
       }];

    [alertController addAction:cancelAction];
    [controller presentViewController:alertController animated:YES completion:nil];
}

-(void)showAlertMessage:(NSString *)strMsg WithType:(ISAlertType)type
{
    [ISMessages showCardAlertWithTitle:AppName message:strMsg iconImage:nil duration:3.f hideOnSwipe:YES hideOnTap:YES alertType:type alertPosition:ISAlertPositionBottom];
}

#pragma mark
#pragma mark - Create NMMTRequest

-(NSMutableURLRequest *)createNMMTImageRequestWithURL:(NSString *)strURL andParameters:(NSMutableDictionary *)dicParams
{
    // create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:100];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in dicParams)
    {
        if ([[dicParams objectForKey:param] isKindOfClass:[UIImage class]])
        {
            NSData *imageData = UIImageJPEGRepresentation([dicParams objectForKey:param], 1.0);
            if (imageData) {
                [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:imageData];
                [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
            }
        }
        else
        {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@\r\n", [dicParams objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];

        }
    }
    // add image data
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set URL
    [request setURL:[NSURL URLWithString:strURL]];
    
    return request;
}

#pragma mark
#pragma mark - Create Request Methods

-(NSMutableURLRequest *)createRequestWithURL:(NSString *)strURL andParameters:(NSMutableDictionary *)dicParams
{
    //Request Create
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strURL]];
    
    //Content Type
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    //Create Body
    NSMutableData *body = [[NSMutableData alloc] init];
    
    for (int i = 0; i < [dicParams allKeys].count; i++)
    {
        if ([[dicParams objectForKey:[[dicParams allKeys] objectAtIndex:i]] isKindOfClass:[UIImage class]])
        {
            [self setName:[[dicParams allKeys] objectAtIndex:i] withFileName:@"imgnew.jpg" withValue:[dicParams objectForKey:[[dicParams allKeys] objectAtIndex:i]] onBody:body];
        }
        else if ([[dicParams objectForKey:[[dicParams allKeys] objectAtIndex:i]] isKindOfClass:[NSURL class]])
        {
            NSURL *urlStr = [dicParams objectForKey:[[dicParams allKeys] objectAtIndex:i]];
            NSString *strURL = urlStr.absoluteString;
            
            if ([strURL hasSuffix:@"m4a"])
            {
                 [self setVideoName:[[dicParams allKeys] objectAtIndex:i] withFileName:@"myAudiofile.m4a" withValue:[dicParams objectForKey:[[dicParams allKeys] objectAtIndex:i]] onBody:body];
            }
            else
            {
                 [self setVideoName:[[dicParams allKeys] objectAtIndex:i] withFileName:@"myfile.mp4" withValue:[dicParams objectForKey:[[dicParams allKeys] objectAtIndex:i]] onBody:body];
            }
           
        }
        else
        {
            [self setName:[[dicParams allKeys] objectAtIndex:i] withValue:[dicParams objectForKey:[[dicParams allKeys] objectAtIndex:i]] onBody:body];
        }
    }
    
    //Set Body parameters
    [request setHTTPBody:body];
    [request setHTTPMethod:@"POST"];
    
    return request;
}

-(NSMutableURLRequest *)createURLEncodedRequestWithURL:(NSString *)strURL andParameters:(NSMutableDictionary *)dicParams
{
    NSMutableString *strPost = [NSMutableString string];
    
    for (int i = 0; i < [dicParams allKeys].count; i++)
    {
        [strPost appendFormat:@"%@=%@&",[[dicParams allKeys] objectAtIndex:i],[dicParams objectForKey:[[dicParams allKeys] objectAtIndex:i]]];
    }
    
    NSString *strPostData  = strPost;
    
    if ([strPost length] > 0)
    {
        strPostData = [strPost substringToIndex:[strPost length] - 1];
    }
    
    NSData *postData = [strPostData dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:strURL]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    return request;
}

-(NSMutableURLRequest *)createNMMTGETRequestWithURL:(NSString *)strURL
{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:strURL]];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    return request;
}

-(NSMutableURLRequest *)createNMMTRequestWithURL:(NSString *)strURL andParameters:(NSMutableDictionary *)dicParams
{
    NSMutableString *strPost = [NSMutableString string];
    
    for (int i = 0; i < [dicParams allKeys].count; i++)
    {
        [strPost appendFormat:@"%@=%@&",[[dicParams allKeys] objectAtIndex:i],[dicParams objectForKey:[[dicParams allKeys] objectAtIndex:i]]];
    }
    
    NSString *strPostData  = strPost;
    
    if ([strPost length] > 0)
    {
        strPostData = [strPost substringToIndex:[strPost length] - 1];
    }
    
    NSData *postData = [strPostData dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:strURL]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    return request;
}

-(NSMutableURLRequest *)createJsonRequestWithURL:(NSString *)strURL andData:(NSData *)requestData
{
    //Request Create
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strURL]];
    
    //Content Type
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody: requestData];
    return request;
}

#pragma mark
#pragma mark - Set Parameter Methods

-(void)setName:(NSString *)name withValue:(NSString *)value onBody:(NSMutableData *)body
{
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",name] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[value dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
}

-(void)setName:(NSString *)name withFileName:(NSString *)fileName withValue:(UIImage *)img onBody:(NSMutableData *)body
{
    CGFloat compression = 0.1f;
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    
    NSData *compresed_data = UIImageJPEGRepresentation(img, compression);
    
    if(compresed_data != nil)
    {
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n",name,fileName] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:compresed_data]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
}

-(void)setVideoName:(NSString *)name withFileName:(NSString *)fileName withValue:(NSURL *)vidURL onBody:(NSMutableData *)body
{
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    
    NSData *compresed_data = [NSData dataWithContentsOfURL:vidURL];
    
    if(compresed_data != nil)
    {
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n",name,fileName] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:compresed_data]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
}


#pragma mark
#pragma mark - Base 64 Encode and Decode

-(NSString *)encodeString:(NSString *)str
{
    return [[str dataUsingEncoding:NSUTF8StringEncoding] base64EncodedStringWithOptions:0];
}

-(NSString *)decodeString:(NSString *)str
{
    return [[NSString alloc] initWithData:[[NSData alloc] initWithBase64EncodedString:str options:0] encoding:NSUTF8StringEncoding];
}

-(NSString *)getXIBPostFix
{
    if (IS_IPHONE)
    {
        if (IS_IPHONE_4)
        {
            return @"";
        }
        else if (IS_IPHONE_5)
        {
            return @"";
        }
        else if (IS_IPHONE_6)
        {
            return @"";
        }
        else
        {
            return @"";
        }
    }
    else
    {
        return @"";
//        return @"_iPad";
    }
}

#pragma mark
#pragma mark - Email Validation

-(BOOL)isValidEmailAddress:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

-(BOOL)isValidEmailOrContact:(NSString *)checkString
{
    if ([self isValidContactNumber:checkString])
    {
        if (checkString.length != 10)
        {
            return NO;
        }
        return YES;
    }
    else
    {
        if ([self isValidEmailAddress:checkString])
        {
            return YES;
        }
        else
        {
            return NO;
        }
    }
}

-(BOOL)isValidContactNumber:(NSString *)checkString
{
    NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    if ([checkString rangeOfCharacterFromSet:notDigits].location == NSNotFound)
    {
        // newString consists only of the digits 0 through 9
        return YES;
    }
    else
    {
        return NO;
    }
}

#define ACCEPTABLE_CHARACTERS @" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"

-(BOOL)isValidStringWithoutSpecialCharacters:(NSString *)checkString
{
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
    
    NSString *filtered = [[checkString componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    
    return [checkString isEqualToString:filtered];
}

#pragma mark
#pragma mark - Empty Validation

-(BOOL)isAnyFieldEmpty:(UIView *)viewFields
{
    //Empty Field Validation
    BOOL Success = NO;
    for (int i = 0; i < viewFields.subviews.count; i++)
    {
        UIView *view = [viewFields.subviews objectAtIndex:i];
        
        if ([view isKindOfClass:[UITextField class]])
        {
            UITextField *txtField = (UITextField *)view;
            
            if ([txtField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0)
            {
                Success = YES;
                break;
            }
        }
        
        if ([view isKindOfClass:[UITextView class]])
        {
            UITextView *txtView = (UITextView *)view;
            
            if ([txtView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0)
            {
                Success = YES;
                break;
            }
        }
    }
    
    return Success;
}

-(BOOL)isAnyFieldEmpty:(UIView *)viewFields withSkipField:(id)objectView
{
    //Empty Field Validation
    BOOL Success = NO;
    for (int i = 0; i < viewFields.subviews.count; i++)
    {
        UIView *view = [viewFields.subviews objectAtIndex:i];
        
        if ([view isKindOfClass:[UITextField class]])
        {
            UITextField *txtField = (UITextField *)view;
            
            if ([txtField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0)
            {
                if (txtField != objectView)
                {
                    Success = YES;
                    break;
                }
            }
        }
        
        if ([view isKindOfClass:[UITextView class]])
        {
            UITextView *txtView = (UITextView *)view;
            
            if ([txtView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0)
            {
                Success = YES;
                break;
            }
        }
        
    }
    
    return Success;
}

#pragma mark
#pragma mark - Resign All Responders

-(void)resignAllViewResponders:(UIView *)viewFields
{    
    for (int i = 0; i < viewFields.subviews.count; i++)
    {
        UIView *view = [viewFields.subviews objectAtIndex:i];
        
        if ([view isKindOfClass:[UITextField class]])
        {
            UITextField *txtField = (UITextField *)view;
            
            [txtField resignFirstResponder];
        }
        
        if ([view isKindOfClass:[UITextView class]])
        {
            UITextView *txtView = (UITextView *)view;
            
            [txtView resignFirstResponder];
        }
    }
}

#pragma mark
#pragma mark - ADD border

-(void)addBorderToView:(UIView *)viewBorder withColor:(UIColor *)clr
{
    viewBorder.layer.borderColor = clr.CGColor;
    viewBorder.layer.borderWidth = 2.0;
}


#pragma mark
#pragma mark - Check Null

-(NSString *)checkNull:(NSString *)str
{
    if ([str isEqual:[NSNull null]] || [str isEqualToString:@"<null>"])
    {
        return @"";
    }
    else
    {
        return str;
    }
}

#pragma mark
#pragma mark - Get Date String

-(NSString *)getDateString:(NSString *)strFrom
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate *newDate = [formatter dateFromString:strFrom];

    NSDateComponents *otherDay = [[NSCalendar currentCalendar] components:NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:newDate];
    NSDateComponents *today = [[NSCalendar currentCalendar] components:NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:[NSDate date]];
    if ([today day] == [otherDay day] && [today month] == [otherDay month] && [today year] == [otherDay year] && [today era] == [otherDay era])
        [formatter setDateFormat:@"HH:mm a"];
    else
        [formatter setDateFormat:@"dd-MM-yyyy"];
    
    return [formatter stringFromDate:newDate];
}


#pragma mark
#pragma mark - Get Comma Seprated String From Array

-(NSMutableString *)getCommaSepratedStringFromArray:(NSMutableArray *)arrData
{
    NSMutableString *str = [NSMutableString string];
    
    for (int i = 0; i < arrData.count; i++)
    {
        if (i==0)
            [str appendString:[arrData objectAtIndex:i]];
        else
            [str appendFormat:@",%@",[arrData objectAtIndex:i]];
    }
    
    return str;
}

-(BOOL)checkFileExistAtPathWithFileName:(NSString *)strFileName
{
    // Build the path, and create if needed.
    NSString* filePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* fileName = strFileName;
    NSString* fileAtPath = [filePath stringByAppendingPathComponent:fileName];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:fileAtPath])
    {
        return NO;
    }
    else
    {
        return YES;
    }
}

- (void)writeStringToFile:(NSString*)aString withFileName:(NSString *)strFileName
{
    // Build the path, and create if needed.
    NSString* filePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* fileName = strFileName;
    NSString* fileAtPath = [filePath stringByAppendingPathComponent:fileName];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:fileAtPath])
    {
        [[NSFileManager defaultManager] createFileAtPath:fileAtPath contents:nil attributes:nil];
    }
    
    // The main act...
    [[aString dataUsingEncoding:NSUTF8StringEncoding] writeToFile:fileAtPath atomically:NO];
}

- (NSString*)readStringFromFile:(NSString *)strFileName
{
    // Build the path...
    NSString* filePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* fileName = strFileName;
    NSString* fileAtPath = [filePath stringByAppendingPathComponent:fileName];
    
    // The main act...
    return [[NSString alloc] initWithData:[NSData dataWithContentsOfFile:fileAtPath] encoding:NSUTF8StringEncoding];
}

#pragma mark
#pragma mark - Get Device Token

-(NSString *)getDeviceToken
{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:KeyDeviceToken]) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:KeyDeviceToken];
    }
    else
    {
        return @" ";
    }
}

#pragma mark
#pragma mark - For image Snapshots

-(UIImage *)getImageFromText:(NSString *)strText ForImageView:(AsyncImageView *)imgView
{
    NSDictionary *textAttributes = @{ NSFontAttributeName: FontMediumWithSize(16),NSForegroundColorAttributeName: [UIColor whiteColor]};
    
    return [self imageSnapshotFromText:strText backgroundColor:getColor(93, 93, 93, 1) circular:YES textAttributes:textAttributes forAsyncImageView:imgView];
}

- (UIImage *)imageSnapshotFromText:(NSString *)text backgroundColor:(UIColor *)color circular:(BOOL)isCircular textAttributes:(NSDictionary *)textAttributes forAsyncImageView:(AsyncImageView *)imgView {
    
    NSMutableString *displayString = [NSMutableString stringWithString:@""];
    
    NSMutableArray *words = [[text componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] mutableCopy];
    
    //
    // Get first letter of the first and last word
    //
    if ([words count]) {
        NSString *firstWord = [words firstObject];
        if ([firstWord length]) {
            // Get character range to handle emoji (emojis consist of 2 characters in sequence)
            NSRange firstLetterRange = [firstWord rangeOfComposedCharacterSequencesForRange:NSMakeRange(0, 1)];
            [displayString appendString:[firstWord substringWithRange:firstLetterRange]];
        }
        
//        if ([words count] >= 2) {
//            NSString *lastWord = [words lastObject];
//            
//            while ([lastWord length] == 0 && [words count] >= 2) {
//                [words removeLastObject];
//                lastWord = [words lastObject];
//            }
//            
//            if ([words count] > 1) {
//                // Get character range to handle emoji (emojis consist of 2 characters in sequence)
//                NSRange lastLetterRange = [lastWord rangeOfComposedCharacterSequencesForRange:NSMakeRange(0, 1)];
//                [displayString appendString:[lastWord substringWithRange:lastLetterRange]];
//            }
//        }
    }
    
    text = [displayString uppercaseString];
    
    CGFloat scale = [UIScreen mainScreen].scale;
    
    CGSize size = imgView.bounds.size;
    if (imgView.contentMode == UIViewContentModeScaleToFill ||
        imgView.contentMode == UIViewContentModeScaleAspectFill ||
        imgView.contentMode == UIViewContentModeScaleAspectFit ||
        imgView.contentMode == UIViewContentModeRedraw)
    {
        size.width = floorf(size.width * scale) / scale;
        size.height = floorf(size.height * scale) / scale;
    }
    
    UIGraphicsBeginImageContextWithOptions(size, NO, scale);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (isCircular) {
        //
        // Clip context to a circle
        //
        CGPathRef path = CGPathCreateWithEllipseInRect(imgView.bounds, NULL);
        CGContextAddPath(context, path);
        CGContextClip(context);
        CGPathRelease(path);
    }
    
    //
    // Fill background of context
    //
    CGContextSetFillColorWithColor(context, color.CGColor);
    CGContextFillRect(context, CGRectMake(0, 0, size.width, size.height));
    
    //
    // Draw text in the context
    //
    CGSize textSize = [text sizeWithAttributes:textAttributes];
    CGRect bounds = imgView.bounds;
    
    [text drawInRect:CGRectMake(bounds.size.width/2 - textSize.width/2,
                                bounds.size.height/2 - textSize.height/2,
                                textSize.width,
                                textSize.height)
      withAttributes:textAttributes];
    
    UIImage *snapshot = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return snapshot;
}

#pragma mark
#pragma mark - Address Functions

//For Addresses
-(void)storeAddresses:(NSArray *)arrAddress
{
//    NSMutableArray *arrOldAddresses = [[NSUserDefaults standardUserDefaults] objectForKey:KeyAddresses];
    
//    NSInteger selectedId = 0;
//    
//    if (arrOldAddresses.count > 0)
//    {
//        NSArray *arrRecords = [arrOldAddresses filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"selected == %@",@"1"]];
//        
//        if (arrRecords.count > 0)
//            selectedId = [[NSString stringWithFormat:@"%@",[[arrRecords objectAtIndex:0] valueForKey:@"id"]] integerValue];
//    }
    
    NSMutableArray *arrNewAddresses = [[NSMutableArray alloc]init];
    
    for (int i = 0; i < arrAddress.count; i++)
    {
        NSMutableDictionary *dicAddress = [[NSMutableDictionary alloc]initWithDictionary:[arrAddress objectAtIndex:i]];
        
        if ([[[arrAddress objectAtIndex:i] valueForKey:@"is_current"] integerValue] == 1)
        {
            [dicAddress setObject:@"1" forKey:@"selected"];
        }
        else
            [dicAddress setObject:@"0" forKey:@"selected"];
    
        [arrNewAddresses addObject:dicAddress];
    }
    
//    if (selectedId >= 0 && arrNewAddresses.count > 0)
//    {
//         NSMutableDictionary *dicAddress = [arrNewAddresses objectAtIndex:0];
//        
//        [dicAddress setObject:@"1" forKey:@"selected"];
//    }
    
    [[NSUserDefaults standardUserDefaults] setObject:arrNewAddresses forKey:KeyAddresses];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSMutableArray *)selectAddressOfId:(NSString *)strId
{
    NSMutableArray *arrAddresses = [[NSUserDefaults standardUserDefaults] objectForKey:KeyAddresses];
    
    NSMutableArray *arrNewAddresses = [[NSMutableArray alloc]init];
    
    for (int i = 0; i < arrAddresses.count; i++)
    {
        NSMutableDictionary *dicAddress = [[NSMutableDictionary alloc]initWithDictionary:[arrAddresses objectAtIndex:i]];
        
        if ([strId isEqualToString:[[arrAddresses objectAtIndex:i] valueForKey:@"id"]])
        {
            [dicAddress setObject:@"1" forKey:@"selected"];
        }
        else
            [dicAddress setObject:@"0" forKey:@"selected"];
        
        [arrNewAddresses addObject:dicAddress];
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:arrNewAddresses forKey:KeyAddresses];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    return arrNewAddresses;
}

-(NSString *)getPinCodeOfSelectedAddress
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:KeyUserLoggedIn] intValue] == 1)
    {
        NSMutableArray *arrOldAddresses = [[NSUserDefaults standardUserDefaults] objectForKey:KeyAddresses];
        NSArray *arrRecords = [arrOldAddresses filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"selected == %@",@"1"]];
        
        if (arrRecords.count > 0)
        {
            return [[arrRecords objectAtIndex:0] valueForKey:@"pincode_id"];
        }
        else
        {
            return @"0";
        }
    }
    else
    {
//        if ([[NSUserDefaults standardUserDefaults] objectForKey:KeyPincode])
//        {
//            return [[NSUserDefaults standardUserDefaults] objectForKey:KeyPincode];
//        }
//        else
//        {
//            return @"0";
//        }
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:KeyPincodeID])
        {
            return [[NSUserDefaults standardUserDefaults] objectForKey:KeyPincodeID];
        }
        else
        {
            return @"0";
        }
    }
}

-(NSString *)getSelectedDisplayTextOfAddress
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:KeyUserLoggedIn] intValue] == 1)
    {
        NSMutableArray *arrOldAddresses = [[NSUserDefaults standardUserDefaults] objectForKey:KeyAddresses];
        NSArray *arrRecords = [arrOldAddresses filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"selected == %@",@"1"]];
        
        if (arrRecords.count > 0)
        {
            return [NSString stringWithFormat:@"%@ - %@",[[arrRecords objectAtIndex:0] valueForKey:@"area"],[[arrRecords objectAtIndex:0] valueForKey:@"pincode"]];
        }
        else
        {
            return @"";
        }
    }
    else
    {
        if ([[NSUserDefaults standardUserDefaults] objectForKey:KeyPincode])
        {
            return [NSString stringWithFormat:@"%@ - %@",[[NSUserDefaults standardUserDefaults] objectForKey:KeyArea],[[NSUserDefaults standardUserDefaults] objectForKey:KeyPincode]];
        }
        else
        {
            return @"";
        }
    }
}

-(NSString *)getSelectedAddress
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:KeyUserLoggedIn] intValue] == 1)
    {
        NSMutableArray *arrOldAddresses = [[NSUserDefaults standardUserDefaults] objectForKey:KeyAddresses];
        NSArray *arrRecords = [arrOldAddresses filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"selected == %@",@"1"]];
        
        if (arrRecords.count > 0)
        {
            return [NSString stringWithFormat:@"%@, %@, %@, %@, %@ - %@",[[arrRecords objectAtIndex:0] valueForKey:@"address_line1"],[[arrRecords objectAtIndex:0] valueForKey:@"address_line2"],[[arrRecords objectAtIndex:0] valueForKey:@"area"],[[arrRecords objectAtIndex:0] valueForKey:@"landmark"],[[arrRecords objectAtIndex:0] valueForKey:@"city_name"],[[arrRecords objectAtIndex:0] valueForKey:@"pincode"]];
        }
        else
        {
            return @"Welcome to Dehleez";
        }
    }
    else
    {
        if ([[NSUserDefaults standardUserDefaults] objectForKey:KeyPincode])
        {
            return [NSString stringWithFormat:@"%@ - %@",[[NSUserDefaults standardUserDefaults] objectForKey:KeyArea],[[NSUserDefaults standardUserDefaults] objectForKey:KeyPincode]];
        }
        else
        {
            return @"Welcome to Dehleez";
        }
    }
}

-(NSDictionary *)getSelectedAddressDetail
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:KeyUserLoggedIn] intValue] == 1)
    {
        NSMutableArray *arrOldAddresses = [[NSUserDefaults standardUserDefaults] objectForKey:KeyAddresses];
        NSArray *arrRecords = [arrOldAddresses filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"selected == %@",@"1"]];
        
        if (arrRecords.count > 0)
            return [arrRecords objectAtIndex:0];
        else
            return nil;
    }
    else
        return nil;
}

#pragma mark
#pragma mark - Database Related Functions

-(void)addToCart:(NSMutableDictionary *)dicProduct
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:KeyUserLoggedIn] integerValue] == 1)
    {
        [self addSingleCartAPIWithProductID:[dicProduct valueForKey:@"id"]];
    }
    
    NSMutableArray *arrProducts = [GLOBALDATABASE requestWith_SelectQuery:[NSString stringWithFormat:@"select Product_qty from ProductCart where Product_API_id = %@",[dicProduct valueForKey:@"id"]]];
    
    NSMutableString *strQuery = [NSMutableString string];
    
    if (arrProducts.count > 0)
    {
        NSLog(@"Before Update : %@",arrProducts);
        NSInteger qty = [[[arrProducts objectAtIndex:0] valueForKey:@"Product_qty"] integerValue];
        qty++;
        [strQuery appendFormat:@"UPDATE ProductCart SET Product_qty = %ld,Product_brand_id = '%@',Product_cat_id = '%@',Product_currency_symbol = '%@',Product_discount = '%@',Product_group_id = '%@',Product_image = '%@',Product_offer_symbol = '%@',Product_price = '%@',Product_name = '%@',Product_stock = '%@' WHERE Product_API_id = %@",qty,[dicProduct valueForKey:@"brand_id"],[dicProduct valueForKey:@"category_id"],[dicProduct valueForKey:@"currency_symbol"],[dicProduct valueForKey:@"discount"],[dicProduct valueForKey:@"group_id"],[dicProduct valueForKey:@"image"],[dicProduct valueForKey:@"offer_symbol"],[dicProduct valueForKey:@"price"],[dicProduct valueForKey:@"product_name"],[dicProduct valueForKey:@"stock"],[dicProduct valueForKey:@"id"]];
        
        NSLog(@"Query : %@",strQuery);
    }
    else
    {
        //Value Params
        [strQuery appendString:@"INSERT INTO ProductCart (Product_API_id,Product_brand_id,Product_cat_id,Product_currency_symbol,Product_discount,Product_group_id,Product_image,Product_offer_symbol,Product_price,Product_name,Product_stock,Product_qty) VALUES ("];
        
        //Value Format Speicifers
        [strQuery appendFormat:@"'%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",[dicProduct valueForKey:@"id"],[dicProduct valueForKey:@"brand_id"],[dicProduct valueForKey:@"category_id"],[dicProduct valueForKey:@"currency_symbol"],[dicProduct valueForKey:@"discount"],[dicProduct valueForKey:@"group_id"],[dicProduct valueForKey:@"image"],[dicProduct valueForKey:@"offer_symbol"],[dicProduct valueForKey:@"price"],[dicProduct valueForKey:@"product_name"],[dicProduct valueForKey:@"stock"],@"1"];
    }
    
    [GLOBALDATABASE requestWith_ExecuteQuery:strQuery];
    
     NSLog(@"After Update : %@",[GLOBALDATABASE requestWith_SelectQuery:[NSString stringWithFormat:@"select Product_qty from ProductCart where Product_API_id = %@",[dicProduct valueForKey:@"id"]]]);
    
    [[NSNotificationCenter defaultCenter] postNotificationName:CartBadgeManageNotification object:nil];
}

-(void)removeFromCart:(NSMutableDictionary *)dicProduct
{
    NSMutableArray *arrProducts = [GLOBALDATABASE requestWith_SelectQuery:[NSString stringWithFormat:@"select Product_qty from ProductCart where Product_API_id = %@",[dicProduct valueForKey:@"id"]]];
    
    if (arrProducts.count > 0)
    {
        NSMutableString *strQuery = [NSMutableString string];
        
        NSInteger qty = [[[arrProducts objectAtIndex:0] valueForKey:@"Product_qty"] integerValue];
        qty--;
        
        if (qty == 0)
        {
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:KeyUserLoggedIn] integerValue] == 1)
            {
                [self removeSingleCartAPIWithProductID:[dicProduct valueForKey:@"id"] isLastQuantity:@"1"];
            }
            
            [strQuery appendFormat:@"DELETE FROM ProductCart WHERE Product_API_id = %@",[dicProduct valueForKey:@"id"]];
        }
        else
        {
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:KeyUserLoggedIn] integerValue] == 1)
            {
                [self removeSingleCartAPIWithProductID:[dicProduct valueForKey:@"id"] isLastQuantity:@"0"];
            }
            
            [strQuery appendFormat:@"UPDATE ProductCart SET Product_qty = %ld WHERE Product_API_id = %@",qty,[dicProduct valueForKey:@"id"]];
        }
        
        [GLOBALDATABASE requestWith_ExecuteQuery:strQuery];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:CartBadgeManageNotification object:nil];
}

#pragma mark
#pragma mark - Get Cart and Sync

-(void)getMyCartAndSync
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:KeyUserLoggedIn] integerValue] == 1) {
        if (![APPDELEGATE checkInternet])
        {
            [GLOBALMETHODS showAlertMessage:@"There is no internect conection. Please try again later!" WithType:ISAlertTypeError];
        }
        else
        {
            NSMutableDictionary *dicInParams = [[NSMutableDictionary alloc] init];
            
            [dicInParams setObject:[[NSUserDefaults standardUserDefaults] objectForKey:KeyUserID] forKey:@"customer_id"];
            [dicInParams setObject:[self getPinCodeOfSelectedAddress] forKey:@"pincode_id"];
            
            NSMutableURLRequest *request = [self createRequestWithURL:[NSString stringWithFormat:@"%@%@",BASEURL,GetMyCartAPIName] andParameters:dicInParams];
            
            CustomURLConnection *connection = [[CustomURLConnection alloc] initWithRequest:request withTag:GetMyCartAPITag];
            [connection setDelegate:self];
            [connection startConnection];
        }
    }
}

-(void)syncWithLocalCartDatabaseFromArray:(NSArray *)arrCart
{
    NSMutableArray *arrCartProducts = [GLOBALDATABASE requestWith_SelectQuery:@"select * from ProductCart"];
    
    if (arrCartProducts.count == 0 && arrCart.count == 0) {
        return;
    }
    
    if (arrCartProducts.count == arrCart.count)
    {
        for (int i = 0; i < arrCartProducts.count; i++)
        {
            NSArray *arrResult = [arrCart filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"product_id == %@",[[arrCartProducts objectAtIndex:i] valueForKey:@"Product_API_id"]]];
            
            if (arrResult.count == 0)
            {
                [self showMessageAndReplaceTheCartWithArray:arrCart];
                break;
            }
            else
            {
                if ([[[arrResult objectAtIndex:0] valueForKey:@"quantity"] integerValue] != [[[arrCartProducts objectAtIndex:i] valueForKey:@"Product_qty"] integerValue])
                {
                    [self showMessageAndReplaceTheCartWithArray:arrCart];
                    break;
                }
            }
        }
    }
    else
    {
        //Put Message that cart has changed and replace the cart in local
        [self showMessageAndReplaceTheCartWithArray:arrCart];
    }
}

-(void)removeCompleteCart
{
    NSMutableArray *arrCartProducts = [GLOBALDATABASE requestWith_SelectQuery:@"select * from ProductCart"];
    
    if (arrCartProducts.count == 0) {
        return;
    }

    [self showAlertMessage:@"Your cart has changed!" WithType:ISAlertTypeInfo];
    
    [GLOBALDATABASE requestWith_ExecuteQuery:@"delete from ProductCart"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:CartChangedNotification object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:CartBadgeManageNotification object:nil];
}

-(void)showMessageAndReplaceTheCartWithArray:(NSArray *)arrCart
{
    [self showAlertMessage:@"Your cart has changed!" WithType:ISAlertTypeInfo];
    
    [GLOBALDATABASE requestWith_ExecuteQuery:@"delete from ProductCart"];
    
    for (int i = 0; i < arrCart.count; i++)
    {
        NSMutableString *strQuery = [NSMutableString string];
        
        [strQuery appendString:@"INSERT INTO ProductCart (Product_API_id,Product_brand_id,Product_cat_id,Product_currency_symbol,Product_discount,Product_group_id,Product_image,Product_offer_symbol,Product_price,Product_name,Product_stock,Product_qty) VALUES ("];
        
        //Value Format Speicifers
        [strQuery appendFormat:@"'%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",[[arrCart objectAtIndex:i] valueForKey:@"product_id"],[[arrCart objectAtIndex:i] valueForKey:@"brand_id"],[[arrCart objectAtIndex:i] valueForKey:@"category_id"],[[arrCart objectAtIndex:i] valueForKey:@"currency_symbol"],[[arrCart objectAtIndex:i] valueForKey:@"discount"],[[arrCart objectAtIndex:i] valueForKey:@"group_id"],[[arrCart objectAtIndex:i] valueForKey:@"image"],[[arrCart objectAtIndex:i] valueForKey:@"offer_symbol"],[[arrCart objectAtIndex:i] valueForKey:@"price"],[[arrCart objectAtIndex:i] valueForKey:@"name"],[[arrCart objectAtIndex:i] valueForKey:@"stock"],[[arrCart objectAtIndex:i] valueForKey:@"quantity"]];
        
        NSLog(@"Query : %@",strQuery);
        
        [GLOBALDATABASE requestWith_ExecuteQuery:strQuery];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:CartChangedNotification object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:CartBadgeManageNotification object:nil];
}

#pragma mark
#pragma mark - Call Single Cart API

-(void)addSingleCartAPIWithProductID:(NSString *)strProductID
{
    if (![APPDELEGATE checkInternet])
    {
        [GLOBALMETHODS showAlertMessage:@"There is no internect conection. Please try again later!" WithType:ISAlertTypeError];
    }
    else
    {
        NSMutableDictionary *dicInParams = [[NSMutableDictionary alloc] init];
        
        [dicInParams setObject:strProductID forKey:@"product_id"];
        [dicInParams setObject:@"1" forKey:@"quantity"];
        [dicInParams setObject:[[NSUserDefaults standardUserDefaults] objectForKey:KeyUserID] forKey:@"customer_id"];
        [dicInParams setObject:[GLOBALMETHODS getDeviceToken] forKey:@"device_id"];
        [dicInParams setObject:[self getPinCodeOfSelectedAddress] forKey:@"pincode_id"];
        
        NSMutableURLRequest *request = [self createRequestWithURL:[NSString stringWithFormat:@"%@%@",BASEURL,AddSingleCartAPIName] andParameters:dicInParams];
        
        CustomURLConnection *connection = [[CustomURLConnection alloc] initWithRequest:request withTag:AddSingleCartAPITag];
        [connection setDelegate:self];
        [connection startConnection];
    }
}

-(void)removeSingleCartAPIWithProductID:(NSString *)strProductID isLastQuantity:(NSString *)strLast
{
    if (![APPDELEGATE checkInternet])
    {
        [GLOBALMETHODS showAlertMessage:@"There is no internect conection. Please try again later!" WithType:ISAlertTypeError];
    }
    else
    {
        NSMutableDictionary *dicInParams = [[NSMutableDictionary alloc] init];
        
        [dicInParams setObject:strProductID forKey:@"product_id"];
        [dicInParams setObject:[[NSUserDefaults standardUserDefaults] objectForKey:KeyUserID] forKey:@"customer_id"];
        [dicInParams setObject:[self getPinCodeOfSelectedAddress] forKey:@"pincode_id"];
        [dicInParams setObject:strLast forKey:@"is_last"];
        
        NSMutableURLRequest *request = [self createRequestWithURL:[NSString stringWithFormat:@"%@%@",BASEURL,RemoveSingleCartAPIName] andParameters:dicInParams];
        
        CustomURLConnection *connection = [[CustomURLConnection alloc] initWithRequest:request withTag:RemoveSingleCartAPITag];
        [connection setDelegate:self];
        [connection startConnection];
    }
}

#pragma mark
#pragma mark - Currency Hex to Symbol

- (NSString *)stringByConvertingHexToSymbol:(NSString *)str {
    
    if (!str || [str isEqual:[NSNull null]] || str.length == 0) {
        return @" ";
    }
    
    NSUInteger myLength = [str length];
    NSUInteger ampIndex = [str rangeOfString:@"&" options:NSLiteralSearch].location;
    
    // Short-circuit if there are no ampersands.
    if (ampIndex == NSNotFound) {
        return str;
    }
    // Make result string with some extra capacity.
    NSMutableString *result = [NSMutableString stringWithCapacity:(myLength * 1.25)];
    
    // First iteration doesn't need to scan to & since we did that already, but for code simplicity's sake we'll do it again with the scanner.
    NSScanner *scanner = [NSScanner scannerWithString:str];
    
    [scanner setCharactersToBeSkipped:nil];
    
    NSCharacterSet *boundaryCharacterSet = [NSCharacterSet characterSetWithCharactersInString:@" \t\n\r;"];
    
    do {
        // Scan up to the next entity or the end of the string.
        NSString *nonEntityString;
        if ([scanner scanUpToString:@"&" intoString:&nonEntityString]) {
            [result appendString:nonEntityString];
        }
        if ([scanner isAtEnd]) {
            goto finish;
        }
        // Scan either a HTML or numeric character entity reference.
        if ([scanner scanString:@"&amp;" intoString:NULL])
            [result appendString:@"&"];
        else if ([scanner scanString:@"&apos;" intoString:NULL])
            [result appendString:@"'"];
        else if ([scanner scanString:@"&quot;" intoString:NULL])
            [result appendString:@"\""];
        else if ([scanner scanString:@"&lt;" intoString:NULL])
            [result appendString:@"<"];
        else if ([scanner scanString:@"&gt;" intoString:NULL])
            [result appendString:@">"];
        else if ([scanner scanString:@"&#" intoString:NULL]) {
            BOOL gotNumber;
            unsigned charCode;
            NSString *xForHex = @"";
            
            // Is it hex or decimal?
            if ([scanner scanString:@"x" intoString:&xForHex]) {
                gotNumber = [scanner scanHexInt:&charCode];
            }
            else {
                gotNumber = [scanner scanInt:(int*)&charCode];
            }
            
            if (gotNumber) {
                [result appendFormat:@"%C", (unichar)charCode];
                
                [scanner scanString:@";" intoString:NULL];
            }
            else {
                NSString *unknownEntity = @"";
                
                [scanner scanUpToCharactersFromSet:boundaryCharacterSet intoString:&unknownEntity];
                
                
                [result appendFormat:@"&#%@%@", xForHex, unknownEntity];
                
                //[scanner scanUpToString:@";" intoString:&unknownEntity];
                //[result appendFormat:@"&#%@%@;", xForHex, unknownEntity];
                NSLog(@"Expected numeric character entity but got &#%@%@;", xForHex, unknownEntity);
                
            }
            
        }
        else {
            NSString *amp;
            
            [scanner scanString:@"&" intoString:&amp];  //an isolated & symbol
            [result appendString:amp];
            
            /*
             NSString *unknownEntity = @"";
             [scanner scanUpToString:@";" intoString:&unknownEntity];
             NSString *semicolon = @"";
             [scanner scanString:@";" intoString:&semicolon];
             [result appendFormat:@"%@%@", unknownEntity, semicolon];
             NSLog(@"Unsupported XML character entity %@%@", unknownEntity, semicolon);
             */
        }
        
    }
    while (![scanner isAtEnd]);
    
finish:
    return result;
}

#pragma mark
#pragma mark - CustomURLConnection Delegate Methods

- (void)customURLConnection:(CustomURLConnection *)connection withTag:(int)tagCon didFailWithError:(NSError *)error
{
    [self showAlertMessage:error.localizedDescription WithType:ISAlertTypeError];
}

//For Exception
- (void)customURLConnection:(CustomURLConnection *)connection withException:(NSException *)exception withTag:(int)tagCon
{
    [self showAlertMessage:exception.description WithType:ISAlertTypeError];
}

- (void)customURLConnectionDidFinishLoading:(CustomURLConnection *)connection withTag:(int)tagCon withResponse:(id)response
{
    NSDictionary *dicResponse = (NSDictionary *)response;
    
    switch (tagCon)
    {
        case AddSingleCartAPITag:
        {
            if ([[dicResponse valueForKey:@"status"] boolValue])
            {
            }
            else
            {
//                [self showAlertMessage:[dicResponse valueForKey:@"message"] WithType:ISAlertTypeError];
            }
        }
            break;
            
        case RemoveSingleCartAPITag:
        {
            if ([[dicResponse valueForKey:@"status"] boolValue])
            {
            }
            else
            {
//                [self showAlertMessage:[dicResponse valueForKey:@"message"] WithType:ISAlertTypeError];
            }
        }
            break;
            
        case GetMyCartAPITag:
        {
            if ([[dicResponse valueForKey:@"status"] boolValue])
            {
                [self syncWithLocalCartDatabaseFromArray:[dicResponse valueForKey:@"data"]];
                NSLog(@"Response : %@",response);
            }
            else
            {
//                [self showAlertMessage:[dicResponse valueForKey:@"message"] WithType:ISAlertTypeError];
                
                [self removeCompleteCart];
            }
        }
            break;
            
        default:
            break;
    }
}


@end
