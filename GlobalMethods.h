

#import <Foundation/Foundation.h>
#import "AppConstants.h"
#import "AsyncImageView.h"
#import "ISMessages.h"
#import "CustomURLConnection.h"

@interface GlobalMethods : NSObject <CustomURLConnectionDelegate>
{
    
}

@property (nonatomic) BOOL isLoggedIn;

@property (nonatomic) BOOL showPass;
@property (nonatomic) BOOL showTicket;
@property (nonatomic) CGRect cartRect;

//Class Methods
+ (GlobalMethods *)sharedInstance;

//Show Alert
-(void)showAlertWithTitle:(NSString *)strTitle andMessage:(NSString *)strMsg withController:(UIViewController *)controller;
-(void)showAlertMessage:(NSString *)strMsg WithType:(ISAlertType)type;

//Create Request
-(NSMutableURLRequest *)createRequestWithURL:(NSString *)strURL andParameters:(NSMutableDictionary *)dicParams;
-(NSMutableURLRequest *)createURLEncodedRequestWithURL:(NSString *)strURL andParameters:(NSMutableDictionary *)dicParams;
-(NSMutableURLRequest *)createJsonRequestWithURL:(NSString *)strURL andData:(NSData *)requestData;
-(NSMutableURLRequest *)createNMMTRequestWithURL:(NSString *)strURL andParameters:(NSMutableDictionary *)dicParams;
-(NSMutableURLRequest *)createNMMTGETRequestWithURL:(NSString *)strURL;
-(NSMutableURLRequest *)createNMMTImageRequestWithURL:(NSString *)strURL andParameters:(NSMutableDictionary *)dicParams;
//String Encodeing decoding methods

-(NSString *)encodeString:(NSString *)str;
-(NSString *)decodeString:(NSString *)str;

//Null Check
-(NSString *)checkNull:(NSString *)str;

//Get XIB Post Fix
-(NSString *)getXIBPostFix;

//TabbarHide
-(BOOL)isValidEmailAddress:(NSString *)checkString;
-(BOOL)isAnyFieldEmpty:(UIView *)viewFields;
-(BOOL)isAnyFieldEmpty:(UIView *)viewFields withSkipField:(id)objectView;
-(void)resignAllViewResponders:(UIView *)viewFields;
-(BOOL)isValidEmailOrContact:(NSString *)checkString;
-(BOOL)isValidContactNumber:(NSString *)checkString;
-(BOOL)isValidStringWithoutSpecialCharacters:(NSString *)checkString;
-(void)addBorderToView:(UIView *)viewBorder withColor:(UIColor *)clr;

-(void)setName:(NSString *)name withValue:(NSString *)value onBody:(NSMutableData *)body;

-(NSMutableString *)getCommaSepratedStringFromArray:(NSMutableArray *)arrData;

//Methods for Local Json

-(BOOL)checkFileExistAtPathWithFileName:(NSString *)strFileName;
- (void)writeStringToFile:(NSString*)aString withFileName:(NSString *)strFileName;
- (NSString*)readStringFromFile:(NSString *)strFileName;

-(NSString *)getDateString:(NSString *)strFrom;

//For Image SnapShots
-(UIImage *)getImageFromText:(NSString *)strText ForImageView:(AsyncImageView *)imgView;

//For Addresses
-(void)storeAddresses:(NSArray *)arrAddress;
-(NSMutableArray *)selectAddressOfId:(NSString *)strId;
-(NSString *)getPinCodeOfSelectedAddress;
-(NSString *)getSelectedDisplayTextOfAddress;
-(NSString *)getSelectedAddress;
-(NSDictionary *)getSelectedAddressDetail;

//Database Related Operations
-(void)addToCart:(NSMutableDictionary *)dicProduct;
-(void)removeFromCart:(NSMutableDictionary *)dicProduct;
-(void)getMyCartAndSync;

-(void)addSingleCartAPIWithProductID:(NSString *)strProductID;
-(void)removeSingleCartAPIWithProductID:(NSString *)strProductID isLastQuantity:(NSString *)strLast;

-(NSString *)getDeviceToken;

- (NSString *)stringByConvertingHexToSymbol:(NSString *)str;

-(void)removeCompleteCart;

@end
